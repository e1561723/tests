const s = require("./stack.js");

let stack = null;

//fonction d'initialisiation de la pile
function initialiser(){
    stack = new s.Stack()
}

// Pour pas besoin de creer une pile a chaque test
beforeEach(() => {
  initialiser();
});

//push(valeur) suivie de peek retourne une valeur 

test('Apres push(valeur) peek() retourne une valeur',()=>{
  //Arrange 
  const stack = new s.Stack ;
  stack.push(10)
  // Act
  const verification = stack.peek()

  // Assert
  expect(verification).toEqual(10);
})


//count() d'une pile nouvelle retourne 0

test('Count() retourne 0 si la pile est vide',()=>{
  //Arrange 
  const stack = new s.Stack ;

  // Act
  const compte = stack.count();

  // Assert
  expect(compte).toEqual(0);
})

//Empty(une pile nouvellement créé) retourne true

test('isEmpty() retourne true si la pile est nouvellement creer',()=>{
    const stack = new s.Stack;

    const empty = stack.isEmpty();
    expect(empty).toBe(true);
})


//Après un push(), isEmpty retourne false

test('Apres push() isEmpty retourne false',()=>{
  const stack = new s.Stack;
  stack.push(10)
  const empty = stack.isEmpty();
  expect(empty).toBe(false);
})

// count() après push() retourne 1 de plus que le count() d'avant
test('Apres push() count() retourn 1 de plus',()=>{
  const stack = new s.Stack;
  const countAvant = stack.count()
  stack.push(10)
  const countApres = stack.count()
  expect(countAvant+1==countApres).toBe(true);
})



// pop() diminue count() de 1
/*
test('Apres pop() count() retourn 1 de moins',()=>{
  const stack = new s.Stack;
  stack.push(10)
  const countApresPush = stack.count()
  stack.pop()
  const countApresPop = stack.count()
  const verification = countApresPush - 1 == countApresPop
  expect(verification).toBe(true);
})
*/



// pop() d'une pile vide retourne null
/*
test('Apres pop() retourne null',()=>{
  const stack = new s.Stack;
  expect(stack.pop()).toBeNull();
})
*/


// peek() d'une pile vide retourne null

test('Apres peek() retourne null si pile vide',()=>{
  const stack = new s.Stack;
  expect(stack.peek()).toBeNull();
})



// peek() change pas le count()
test('Apres peek() le count reste le meme',()=>{
  const stack = new s.Stack;
  const countAvant = stack.count()
  stack.peek()
  const countApres = stack.count()
  expect(countAvant===countApres).toBe(true);
})



// pop() retourne le dernier element empiler avec push()
/*
test('Apres pop() retourne le dernier element empiler avec push()',()=>{
  const stack = new s.Stack;
  const element = 10;
  stack.push(element)
  const verification = stack.pop()
  expect(verification).toEqual(element);
})
*/


// peek() retourn le dernier element empiler avec push()
test('Apres peek() retourne le dernier element empiler avec push()',()=>{
  const stack = new s.Stack;
  const element = 10;
  stack.push(element)
  const verification = stack.peek()
  expect(verification).toEqual(element);
})

// pop() depile 3 elements dans l'ordre inverse qu'il ont été empiler
/*
test('Apres pop() depile 3 elements dans ordre inverse qu il ont ete empiler',()=>{
  const stack = new s.Stack;
  stack.push(10)
  stack.push(11)
  stack.push(12)
  expect(stack.pop()).toEqual(12);
  expect(stack.pop()).toEqual(11);
  expect(stack.pop()).toEqual(10);
})
*/




